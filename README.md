# PIC32MK_examples

Project IDE: MPLAB X v5.05 
Compiler: XC32
Harmony: MPLAB Harmony 2.06

16 MHz crystal oscillator, 120 MHz System clock

UART1 example

CAN transmit M
**Folder must be placed in folder on the path: .\microchip\harmony\v2_06\apps**

CAN + ADC example
CAN ADDR MCU 0x8000
CAN ADDR ADC 0x780n (n ... number of channel 0 ... 9)

CAN + ADC + STATIC_INTERRUPT example
100 ms, 46875 preload



