#include "config.h"
#include "uart.h"

void main(){
    uint32_t cnt = 0;
    
    //  GRESKA NA SEMI! RC7 -> RX, RC6 -> TX
    //  ISPRAVNO! -- RC6 -> RX RC7 -> TX
    
    TRISC |= (1<<6);
    TRISC &= ~(1<<7); 
    
    U1RXRbits.U1RXR = 5;
    RPC7Rbits.RPC7R = 1;
    
    uart1_init(115200);
    
    while(cnt!=250000) cnt++;  
    
    while(1){
        if(uart1_data_ready()==1){
            char c = uart1_getch();
            uart1_putch(c);
        }
    }
}
