#include "uart.h"
/* uart1_init() sets up the UART1 for the most standard and minimal operation
 *  Enable TX and RX lines, 8 data bits, no parity, 1 stop bit, idle when HIGH
 *
 * Input: Desired Baud Rate
 * Output: Actual Baud Rate from baud control register U2BRG after assignment*/
uint32_t uart1_init(uint32_t desired_baud){
    
    U1MODE = 0;         // disable autobaud, TX and RX enabled only, 8N1, idle=HIGH
    U1STA = 0x1400;     // enable TX and RX
    U1BRG = Baud2BRG(desired_baud); // U2BRG = (FPb / (16*baud)) - 1
    U1MODESET = 0x00028000; // enable UART1, use SYSCLK for generating baudrate
    
    // Calculate actual assigned baud rate
    uint32_t actual_baud = SYSCLK / (16 * (U1BRG+1));
 
    return actual_baud;
}

/* uart1_tx_str() transmits a string to the UART1 TX pin MSB first
 *
 * Inputs: *buffer = string to transmit */
uint16_t uart1_tx_str(const char *buffer)
{
    int16_t size = strlen(buffer);
    while(size)
    {
        while(U1STAbits.UTXBF);    // wait while TX buffer full
        U1TXREG = *buffer;          // send single character to transmit buffer
 
        buffer++;                   // transmit next character on following loop
        size--;                     // loop until all characters sent (when size = 0)
    }
 
    while( !U1STAbits.TRMT);        // wait for last transmission to finish
 
    return 0;
}

/* uart1_rx_str() is a blocking function that waits for data on
 *  the UART1 RX buffer and then stores all incoming data into *buffer
 *
 * Note that when a carriage return '\r' is received, a nul character
 *  is appended signifying the strings end
 *
 * Inputs:  *buffer = Character array/pointer to store received data into
 *          max_size = number of bytes allocated to this pointer
 * Outputs: Number of characters received */
uint16_t uart1_rx_str(char *buffer, uint16_t max_size)
{
    uint16_t num_char = 0;
 
    /* Wait for and store incoming data until either a carriage return is received
     *   or the number of received characters (num_chars) exceeds max_size */
    while(num_char < max_size)
    {
        while(!U1STAbits.URXDA);   // wait until data available in RX buffer
        *buffer = U1RXREG;          // empty contents of RX buffer into *buffer pointer
 
        // insert nul character to indicate end of string
        if( *buffer == '\r'){
            *buffer = '\0';     
            break;
        }
 
        buffer++;
        num_char++;
    }
 
    return num_char;
} 


/* uart1_data_ready() - check if data ready to receive
 *  the UART1 RX buffer have data
 *
 *
 * Outputs: 1 - if data is ready for read
 *          0 - if the UART1 RX buffer is empty
 */

uint8_t uart1_data_ready(void){
    return U1STAbits.URXDA;
}

/* uart1_putch() - sends a character on UART1
 *
 * Input: character 
 */
void uart1_putch(char ch){
    U1TXREG = ch;          // send single character to transmit buffer
    
    while( !U1STAbits.TRMT);        // wait for last transmission to finish
}

/* uart1_getch() - receives a character on UART1
 *
 * Output: received character
 */
char uart1_getch(void){
    char c = U1RXREG;
    return c;
}