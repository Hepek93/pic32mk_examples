#ifndef _UART_H    
#define _UART_H

#include <stdint.h>
#include <xc.h>
#include <string.h>

#ifndef SYSCLK
    #define SYSCLK 120000000L
#endif

// Equation to set baud rate from UART reference manual table 22-3 page 334
#define Baud2BRG(desired_baud)      ( (SYSCLK / (16*desired_baud))-1)


uint16_t uart1_tx_str(const char *buffer);
uint16_t uart1_rx_str(char *buffer, uint16_t max_size);
uint32_t uart1_init(uint32_t baud);

uint8_t uart1_data_ready(void);

void uart1_putch(char ch);
char uart1_getch(void);

#endif
